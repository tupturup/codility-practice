"""A non-empty zero-indexed array A consisting of N integers is given.

A permutation is a sequence containing each element from 1 to N once, and only once.

For example, array A such that:

    A[0] = 4
    A[1] = 1
    A[2] = 3
    A[3] = 2
is a permutation, but array A such that:

    A[0] = 4
    A[1] = 1
    A[2] = 3
is not a permutation, because value 2 is missing.

The goal is to check whether array A is a permutation.

Write a function:

def solution(A)
that, given a zero-indexed array A, returns 1 if array A is a permutation and 0 if it is not.

For example, given array A such that:

    A[0] = 4
    A[1] = 1
    A[2] = 3
    A[3] = 2
the function should return 1.

Given array A such that:

    A[0] = 4
    A[1] = 1
    A[2] = 3
the function should return 0.

Assume that:

N is an integer within the range [1..100,000];
each element of array A is an integer within the range [1..1,000,000,000].
Complexity:

expected worst-case time complexity is O(N);
expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
Elements of input arrays can be modified."""


#!/usr/bin/env python

def solution(A):
    a = len(A)
    m = 0
    rep = 0
    l = [0]*a

    for item in A:
        if item <= a: #ako je item veci od a automatski znamo da neki element fali
            if l[item - 1] == 0: #moramo provjeriti ponavljanja; 0 znaci da nismo imali taj element prije
                l[item - 1] = 1
            else: #ovo se dogodi u slucaju da se element ponovio. to i dalje ne znaci da nije permutacija
                rep = 1
                m = item
        else: #mozemo odmah znati da nije permutacija ako m nije jednako 0
            m = item

    # rep == 0 i m == 0 - permutacija; rep == 0 i m !=0 - !permutacija; rep == 1 i m == 0 -permutacija; rep == 1 i m != 0 - !permutacija
    if (m == 0 and rep == 0) or (m == 0 and rep == 1):
        return 1
    else:
        return 0
    pass

solution([1, 2, 4])
